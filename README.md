# Technical Writing team project

The [Technical Writing team](https://about.gitlab.com/handbook/product/ux/technical-writing/)
project contains:

- Epics, issues, and the roadmap for the Technical Writing team and GitLab
  documentation.
- Issues related to team management and technical writing discussions, including
  site design, information architecture, the GitLab style guide, and content
  design.
- Team-specific onboarding resources.
- Scripts that run on a schedule and create the monthly TW plan issue.

## Rotate the `GITLAB_API_PRIVATE_TOKEN`

Once a year, at the beginning of June, we must rotate the `GITLAB_API_PRIVATE_TOKEN` that's
registered in **Settings > CI/CD > Variables**.
The script that creates the monthly milestone planning issue uses this token.

To create a new access token:

1. Visit the [Project access tokens settings page](https://gitlab.com/gitlab-org/technical-writing/-/settings/access_tokens).
1. Select **Add new token**, and fill in the following values:
   - **Token name**: `TW bot`.
   - **Expiration date**: one year from the date you create the token.
   - **Select a role**: `Developer`.
   - **Select scopes**: `api`.
1. Select **Create project access token**.
1. After the token is created, go to **Your new project access token** at the top
   and copy the token value. It should start with `glpat-`.
1. Navigate to the [CI/CD settings](https://gitlab.com/gitlab-org/technical-writing/-/settings/ci_cd),
   expand **Variables**, select **Edit** for the `GITLAB_API_PRIVATE_TOKEN` CI/CD variable, and update the
   **Value** field with the new token.
1. Select **Save changes**.
