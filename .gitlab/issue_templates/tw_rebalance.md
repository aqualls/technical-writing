<!--Name the issue "Technical Writing team rebalancing plan: <month> <year>"-->
We're planning group reassignments of technical writers to:
<!--Add/remove/edit as needed-->
- Rebalance the workload following a change in staffing in the Technical Writing team.
- Rebalance the workload for existing team members.
- Provide Technical Writer coverage for new groups.
- Provide Technical Writer coverage for groups with product investment focus.
- Provide `#docs` channel coverage for groups with lower product investment focus or with low or no ~"Technical Writing" MRs and issues.

We expect the reassignments to come into effect by `<YYYY-MM-DD> (<milestone>)`.

The changes impact the following groups:

- <!--~"devops::secure" ~"group::vulnerability research"-->
- <!--etc-->
- ...

We reorganize semi-frequently because our [TW team](https://about.gitlab.com/company/team/?department=technical-writing) does not yet meet our gearing ratio (1 writer, 3 groups), and we must rebalance the workload.

For this current rebalancing, out of `x` total groups, `x`% of previously existing groups maintain their existing technical writing relationships:

- `x` group(s) get their initial technical writer assignment.
- `x` previously existing groups get new technical writer assignments, based on work balancing and priorities to ensure velocity for their deliverables.
- `x` groups keep their current technical writer.
- `x` groups that previously had a named technical writer will be supported by the [docs channel](https://gitlab.slack.com/archives/C16HYA2P5).
- `x` groups in total are supported by the docs channel.

<!--List and tag all PMs, EMs, and PDMs that are affected by group changes-->
`@PM1 @EM1 @PDM1 @PM2 @EM2 @PDM2 etc.`
<!--Create a simplified version of the rebalancing spreadsheet and link to it here-->
Please take a look at the [technical writing group reassignments](link-to-rebalance-spreadsheet) and, if you like, add questions or comments below in this issue. Please feel free to share this issue more widely. Thank you!

/label ~"Technical Writing" ~"Technical Writing Leadership"
