# Technical Writing recurring tasks for: YYYY-MM

Each month, the Technical Writer [assigned to recurring tasks](https://handbook.gitlab.com/handbook/product/ux/technical-writing/#regularly-scheduled-tasks)
ensures the following tasks are completed to help minimize technical debt.

## Suggested order

These recurring tasks require a varying amount of time to complete. To make efficient use of your time, they are grouped according to the amount of time they usually take.

These tasks can sometimes take a long time:

- Review the Kramdown build logs for warnings.
- Check for broken external links.
- Search for and remove expired redirect files.
- Search for and report on pages not in the global nav.

These tasks usually don't take as much time:

- Look for uncompressed images.
- Check for unlinked images.
- Check for trailing whitespace.

## Local tasks

The following tasks have tests that must be run locally on your workstation. Before performing these tasks, make sure you
have an updated `gitlab-docs` repository:

```shell
make update
```

To perform these tasks:

- [ ] **Look for [uncompressed images](https://docs.gitlab.com/ce/development/documentation/styleguide/index.html#compress-images).**

  <details><summary>Details</summary>

  1. Run the following command in `gitlab-org/gitlab` to check if any uncompressed images exist
     and compress them:

     ```shell
     bin/pngquant compress
     ```

     If you get an `Error: pngquant executable was not detected in the system`, try installing `pngquant` using Homebrew: `brew install pngquant`.

  1. If there are any results, create a new branch and then a merge request.
     Assign to any TW for merge.

  </details>

- [ ] **Search for and remove expired redirect files.**

  <details><summary>Details</summary>

  1. Run the following command in `gitlab-org/gitlab-docs` to check if any redirect
     files need to be removed. This [automatically creates](https://gitlab.com/gitlab-org/gitlab-docs/-/blob/main/doc/raketasks.md#clean-up-redirects)
     MRs in every project that has redirect files that have expired:

     ```shell
     DRY_RUN=true bundle exec rake docs:clean_redirects
     ```

     If the task runs without errors, run the same command without `DRY_RUN=true`:

     ```shell
     bundle exec rake docs:clean_redirects
     ```

     The task only removes the expired redirect files. There might be cases
     where some docs checks fail, so you'll need to fix those yourself. For example:

     - `docs-lint links` error: `Job failed because the deleted file was referenced somewhere else.`

       Check the pipeline error message to identify which files reference the deleted document.
       In each file that references the deleted document, update the URL to the new location
       (the `redirect_to` value).
     - `ui-docs-links-lint` error: `Job failed because the deleted file was referenced in a help link in the UI.`

       Open an MR to update the UI link. When merged, rebase your redirects MR to pick up the fix.
     - `docs-lint markdown` error: `Job failed because the number of filenames or directory names with dashes changed.`

       In the [`lint-doc.sh` file](https://gitlab.com/gitlab-org/gitlab/-/blob/master/scripts/lint-doc.sh),
       update the `FILE_NUMBER_DASHES` or `DIR_NUMBER_DASHES` values to match the new number of filenames
       or directory names with dashes.
       Changes to this file trigger a longer pipeline and require an additional review from Code Owners.

  1. In the branch created in `gitlab-docs`, check the [`content/_data/redirects.yaml`](https://gitlab.com/gitlab-org/gitlab-docs/-/blob/main/content/_data/redirects.yaml)
     for redirects that are very old (usually at the top of the file). Delete any entries
     with a `remove_date:` older than the current date, and push the commit. For example,
     check the [commits in this cleanup MR](https://gitlab.com/gitlab-org/gitlab-docs/-/merge_requests/5040/commits).

  1. Add the `gitlab-docs` MR as a dependency to any MRs that delete the redirect files
     in the other projects. Assign all MRs to the same Technical Writer, and explain that the
     `gitlab-docs` MR should be merged first, followed by the MRs in the other projects.

     Be sure to `@`-mention `hsmith-watson` in Marketing so they can update any now-expired links on
     `about.gitlab.com`.

  </details>

- [ ] **Search for and report on pages not in the global nav.**

  <details><summary>Details</summary>

  1. Run the following command in `gitlab-org/gitlab-docs`:

     ```shell
     make check-pages-not-in-nav
     ```

  1. For each missing page, check with the group's assigned writer to confirm if it should be added to or excluded from
     the global nav.
  1. Open an MR per page to add it to the global nav. Assign the MR to the writer assigned to the group. You don't need to
     add all pages to the global nav, just add a few as you have time.
  1. If you find false positives and:
     - The false positives are individual pages, add `ignore_in_report: true` to the page's metadata.
     - All the files in a directory are false positives, add the directory to
       [`scripts/pages_not_in_nav.js`](https://gitlab.com/gitlab-org/gitlab-docs/-/blob/635d338a420c22bf1ed7a6f888f9572df7309f49/scripts/pages_not_in_nav.js#L63).

  </details>

- [ ] **Search for `<!--- start_remove` and delete any content with dates in the past.**

## Remote tasks

The following tasks have tests that you can run from pipelines in the `gitlab-docs` project.
Some of the tests may not return any results, and no further work is required for that
task. To perform these tasks:

1. Go to the [pipeline schedules page](https://gitlab.com/gitlab-org/gitlab-docs/-/pipeline_schedules).
1. Select **Run** for the `Run TW chores jobs` scheduled pipeline.
1. Go to the [pipelines page](https://gitlab.com/gitlab-org/gitlab-docs/-/pipelines)
   and find the pipeline you just triggered at the top. The pipeline has two stages.

   The first stage runs the `compile_dev` job automatically:

   - [ ] **Review the Kramdown build logs for warnings.** Check the `compile_dev`
     job log, and search for `kramdown warning` messages (which are usually caused
     by malformed Markdown).
     - Ignore warnings for files in the /blueprints and /solutions folders, and de-prioritize
       warnings for content in the /development folder (do other content first).
     - If the job log is truncated, select **Complete Raw** near the top to view the full log.
     - A warning might be reported for a specific line number, but this is often inaccurate. Check several lines above or below.
     - If the warning is reported for line 1 in a file, it's
       likely the problem is in a Markdown table, which could be anywhere in the file.

   The second stage has three manual jobs. Select **Run** on the jobs you want to run:

   - [ ] **Check for unlinked images.** Run the `test_unlinked_images` job, which checks
     all 4 projects to see if any images are no longer in use. It takes 40+ minutes
     to run, so you may want to start this job first, then work on the other tasks
     while waiting.
   - [ ] **Check for broken external links.** Run the `test_external_links_charts`, `test_external_links_gitlab`,
     `test_external_links_gitlab_runner`, `test_external_links_omnibus_gitlab`, and `test_external_links_operator` jobs.
     The jobs can produce a lot of output, so scan through the lists and prioritize the fixes as follows:

     <details><summary>Details</summary>

     - Links returning a `404` error. These links are likely broken
       for customers, so they should be fixed first. If you can't find a replacement
       link, lean toward removing the link and surrounding content.
     - Redirects. At some stage, the redirects might be removed too, so it's better
       to update them now. Fix as many as time allows. When evaluating redirects:
       - Check the forwarded link, as it may also
         be broken or may redirect again to another page.
       - Check if the URL has a forward slash (`/`) at the end when you visit it, and ensure your fix does the same.
         Many redirect warnings are caused solely due to the presence or absence of the slash.
       - Check for section title links (anchors), such as
         `#section-title`. You may need to add the `#section-title` manually.
       - Create an issue and assign it to an SME if the updated link does not provide
         the desired information.
     - False positives. This is where a failing link is actually valid, for example,
       where a website needs authentication, or the site server returns a response
       the link checker sees as a failure. To fix this, exclude it from
       the link checker. Go to <https://gitlab.com/gitlab-org/gitlab-docs/-/blob/main/lychee.toml>
       and add the link in the `exclude = []` list, following the pattern of the other links
       already there. Fix as many as time allows.

     </details>

   - [ ] **Check for trailing whitespace.** Run the `test_EOL_whitespace` job to
     find all pages with lines that have trailing whitespaces and fix them.

     In each project's `doc` or `docs` directory, run:

     ```shell
     markdownlint-cli2 --config .markdownlint/.markdownlint-cli2.yaml '**/*.md'
     ```

     Create a merge request with the fixes and have them merged back to the project's default branch.

If you have problems (such as broken links without obvious replacements), ask in the appropriate
Slack channel, or open an issue/MR. Note that these tasks aren't intended to solve 100% of related
technical debt.

## Final steps

- [ ] **Improve [this template](https://gitlab.com/gitlab-org/technical-writing/-/blob/main/.gitlab/issue_templates/tw-monthly-tasks.md), if needed.**
- [ ] Create a new issue for the next month's chores, and assign it to the next TW
  [on the schedule](https://handbook.gitlab.com/handbook/product/ux/technical-writing/#regularly-scheduled-tasks).

  Link the new issue here:

/label ~Technical Writing
/label ~tw::doing
