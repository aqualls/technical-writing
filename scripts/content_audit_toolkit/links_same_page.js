import fs from "fs";
import path from "path";
import { createObjectCsvWriter } from "csv-writer";

const repo = "gitlab";
const baseDirectoryPath = `../${repo}/doc`;

function findDuplicateLinks(
  markdown,
  fileName,
  csvWriterOccurrences,
  csvWriterLinkText
) {
  const linkMap = new Map();
  const linkCounts = new Map();

  const linkRegex = /\[([^\]]*)\]\((.*?)\)/g;
  const matches = markdown.matchAll(linkRegex);

  for (const match of matches) {
    const linkText = match[1];
    const href = match[2];
    var url = `https://docs.gitlab.com/ee${fileName
      .replace(baseDirectoryPath, "")
      .replace(".md", ".html")}`;

    if (linkMap.has(href)) {
      linkMap.get(href).push(linkText);
    } else {
      linkMap.set(href, [linkText]);
    }

    if (linkCounts.has(href)) {
      linkCounts.set(href, linkCounts.get(href) + 1);
    } else {
      linkCounts.set(href, 1);
    }
  }

  linkMap.forEach((linkTexts, href) => {
    if (linkTexts.length > 1) {
      linkTexts.forEach((linkText) => {
        const linkTextRecord = {
          "Markdown Filename": fileName,
          "Link Target": href,
          "Link Text": linkText,
          URL: url,
        };
        csvWriterLinkText.writeRecords([linkTextRecord]);
      });
    }
  });

  const occurrencesRecords = [];
  linkCounts.forEach((count, href) => {
    if (count > 1) {
      occurrencesRecords.push({
        "Markdown Filename": fileName,
        "Link Target": href,
        Occurrences: count,
        URL: url,
      });
    }
  });

  if (occurrencesRecords.length > 0) {
    csvWriterOccurrences.writeRecords(occurrencesRecords);
  }
}

async function processDirectory(
  directory,
  csvWriterOccurrences,
  csvWriterLinkText
) {
  const files = await fs.promises.readdir(directory);

  for (const file of files) {
    const filePath = path.join(directory, file);
    const fileStat = await fs.promises.stat(filePath);

    if (fileStat.isDirectory()) {
      // If it's a directory, recursively process it
      await processDirectory(filePath, csvWriterOccurrences, csvWriterLinkText);
    } else if (fileStat.isFile() && path.extname(file) === ".md") {
      const markdownContent = await fs.promises.readFile(filePath, "utf-8");
      findDuplicateLinks(
        markdownContent,
        filePath,
        csvWriterOccurrences,
        csvWriterLinkText
      );
    }
  }
}

const csvWriterOccurrences = createObjectCsvWriter({
  path: "duplicate_links_occurrences.csv",
  header: [
    { id: "Markdown Filename", title: "Markdown Filename" },
    { id: "Link Target", title: "Link Target" },
    { id: "Occurrences", title: "Occurrences" },
    { id: "URL", title: "URL" },
  ],
});

const csvWriterLinkText = createObjectCsvWriter({
  path: "duplicate_links_link_text.csv",
  header: [
    { id: "Markdown Filename", title: "Markdown Filename" },
    { id: "Link Target", title: "Link Target" },
    { id: "Link Text", title: "Link Text" },
    { id: "URL", title: "URL" },
  ],
});

processDirectory(baseDirectoryPath, csvWriterOccurrences, csvWriterLinkText)
  .then(() => {
    console.log("CSV files have been generated.");
  })
  .catch((err) => {
    console.error(err);
  });
