.PHONY: all clean

.PHONY: test
test: rubocop

.PHONY: rubocop
rubocop:
	@bundle exec rubocop --parallel

setup:
	@corepack enable
	@gem update --system
	@bundle install
	@yarn install --immutable

# Content audit scripts/helpers
update-all-projects:
	(cd ../gitlab-docs && make update-all-projects)

compile-docs:
	(cd ../gitlab-docs && make compile)

check-pages-not-in-nav:
	@node ./scripts/content_audit_toolkit/pages_not_in_nav.js

check-page-nav-titles:
	@node ./scripts/content_audit_toolkit/navigation_titles.js

check-links-same-page:
	@node ./scripts/content_audit_toolkit/links_same_page.js
